resource "aws_kms_key" "mykey" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 7
}
resource "random_string" "random" {
  length  = 5
  special = false
  numeric = false
  upper   = false
}

resource "aws_s3_bucket" "bucket" {
  bucket = "my-test-bucket-sue-${random_string.random.result}"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "couple" {
  bucket = aws_s3_bucket.bucket.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.mykey.arn
      sse_algorithm     = "aws:kms"
    }
  }
}
resource "aws_secretsmanager_secret" "testsecret" {
  name = "testsecret1-${random_string.random.result}"
}
