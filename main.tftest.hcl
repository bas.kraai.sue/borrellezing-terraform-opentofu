run "key_single_region" {
  assert {
    condition = aws_kms_key.mykey.multi_region == false
    error_message = "Key needs to be region specific"
  }
}
run "secret_check" {
  assert {
    condition = issensitive(resource.aws_kms_key.mykey.id) == false
    error_message = "Secret in the id of the key?"
  }
}
