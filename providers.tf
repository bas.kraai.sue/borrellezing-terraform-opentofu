terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.46.0"
    }
  }
  # encryption {
  #   method "aes_gcm" "aws" {
  #     keys = key_provider.aws_kms.basic
  #   }
  #   state {
  #       enforced = true
  #       method = method.aes_gcm.aws
  #   }
  #   plan {
  #       enforced = true
  #       method = method.aes_gcm.aws
  #   }
  #   key_provider "aws_kms" "basic" {
  #     kms_key_id = "03b2755f-5987-4c3a-83a8-57476908b7c9"
  #     region = "eu-central-1"
  #     key_spec = "AES_256"
  #   }
  # }
}

provider "aws" {
  region = "eu-central-1"
}
